This package (extractors) in the project t2-c2-extractors contains classes used to extract metadata from .dm3 (digital 
micrograph) files. 


#

Before running these extractors, install pyclowder exctactors by

pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git

The extractor t2.c2.image.preview.py generates image previews. For SEM files, it will generate a preview for the TIF
image. For dm3 (digital micrograph) files, it will generate a preview image in greyscale.

The class dm3reader_v072.py was available online and has been modified for our project in the following ways:

1. function 'fix_metadata_tags' was added so that the key/value pairs of 'tags' (a dictionary of  metadata tags) can be 
used in a JSON. It handles encoding, characters that are not alpha numeric,  and problem character such as '.' ,
which cannot be in the key value for a JSON.

2. function 'make_pgm' added which returns a pgm (greyscale image) file for the .dm3 file. A temporary file was created 
using the original script; this returns a file so it can be stored or used as preview.

The class MetadataExctactor is modeled after ImageMetadataExtractor.py by the ncsa.

1. function 'create_dm3_metadata' added. This function takes the metadata tags and uploads them as metadata.

2. the mimetype for this if 'file.image' as different types of image files will be used with this extractor. 


HOW TO USE

1. to run MetadataExctractor.py. With rabbitmq and clowder already started, run that script. 
No command line arguments are required since those values are handled in metadata_extractor_config

2. Run t2.c2.image.preview.py. It requires no arguments in the command line. Any values needed for this file is in config.py

3. when files are uploaded to clowder, the extractor will run. The tags will be included under 'system generated
metadata.' For .dm3 files, a greyscale image preview should be available. 


