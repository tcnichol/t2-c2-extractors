__author__ = 'todd_n'
import h5py
import numpy as np
import os

pathToTextFile = '/Users/todd_n/Desktop/tiff_test_files/tif-1/Mag Calibration Test 2.txt'

textTarget = '/Users/todd_n/Desktop/bytes_test_text.txt'

pathToTifFile = '/Users/todd_n/Desktop/tiff_test_files/tif-1/Mag Calibration Test 2.tif'

tifTarget = '/Users/todd_n/Desktop/bytes_test_tif.tif'

f_txt = open(pathToTextFile,'rb')
bytes = f_txt.read()
f_txt.close()

f_txt_new = open(textTarget,'wb')
f_txt_new.write(bytes)
f_txt_new.close()
f_txt.close()

f_txt_new = open(textTarget,'rb')
new_bytes = f_txt_new.read()
f_txt_new.close()

text_same = (bytes == new_bytes)

f_tif = open(pathToTifFile,'rb')
tif_bytes = f_tif.read()
f_tif.close()

f_tif_new = open(tifTarget,'wb')
f_tif_new.write(tif_bytes)
f_txt_new.close()

f_tif_new = open(tifTarget,'rb')
tif_bytes_new = f_tif_new.read()
f_tif_new.close()

tif_same = (tif_bytes == tif_bytes_new)

try:
    test_hdf5 = None
    if os.path.isfile('/Users/todd_n/Desktop/test.hdf5'):
        test_hdf5 = h5py.File('/Users/todd_n/Desktop/test.hdf5','r+')
    else:
        test_hdf5 = h5py.File('/Users/todd_n/Desktop/test.hdf5','w')
    newFileByteArray = bytearray(tif_bytes)

    test_hdf5.create_dataset('Mag Calibration Test 2.tif',data=newFileByteArray)
    test_hdf5.close()
except BaseException as e:
    print(str(e))

#get tif image, write to see if it is still the same

f = h5py.File('/Users/todd_n/Desktop/test.hdf5','r+')
items = f.items()
data_set = f['Mag Calibration Test 2.tif']
bytes_from_set = data_set.value

test = open('/Users/todd_n/Desktop/test.tif','wb')
test.write(bytes_from_set)
test.close()




print('done')