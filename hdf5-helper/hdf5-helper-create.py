__author__ = 'todd_n'
import h5py
import os
import os.path
import tifffile
import numpy as np

def create_dataset_text(text_files, group):

    entries = []

    if type(text_files) == list:
        for each in text_files:
            if type(each) == file:
                lines = each.readlines()
                lines.insert(0,'.txt')
                entries.append(lines)
            elif type(each) == list:
                #we assume this means that "each" is the result of f.readlines() from a file f
                each.insert(0,'.txt')
                entries.append(each)
    elif type(text_files) == file:
        pass
    return entries

def get_tiff_as_array(pathToTiff):
    tif = tifffile.TiffFile(pathToTiff)
    tif_array = tif.asarray()
    return tif_array

def get_base_name_from_path(pathToFile):
    path_parts = pathToFile.split('/')
    file_name = path_parts[-1]
    file_name = file_name[:file_name.index('.')]
    return file_name

def createHDF5_Tiff(pathToHDF5,pathToTiff, pathToMetadataFile):
    f = None
    if os.path.isfile(pathToHDF5):
        f = h5py.File(pathToHDF5,'r+')
    else:
        f = h5py.File(pathToHDF5,'w')

    base_name = get_base_name_from_path(pathToTiff)
    tif_array = get_tiff_as_array(pathToTiff)

    meta_f = open(pathToMetadataFile,'r')
    lines = meta_f.readlines()

    meta_f.close()

    try:
        f.create_group(base_name)
        f[base_name].create_dataset('tif',data=tif_array)
        f[base_name].create_dataset('txt',data=lines)
        f.close()
    except:
        print("could not create group")
    try:
        f[base_name].create_dataset('tif',data=tif_array)
        f[base_name].create_dataset('txt',data=lines)
        f.close()
    except:
        print("could not overwrite group")

def create_text_from_lines(destination,lines):
    f = open(destination,'w')
    for line in lines:
        f.write(line)
    f.close()

def create_tif_from_array(destination,tif_array):
    tifffile.imsave(destination,data=tif_array)


def unpack_hdf5_tiff(pathToHDF5,destination):
    f = h5py.File(pathToHDF5,'r+')
    items = f.items()
    group_names = []
    for i in range(0,len(items)):
        group_name = items[i][0]
        group = f[group_name]
        text_lines = group['txt'].value
        tif_array = group['tif'].value
        text_file_name = group_name+'.txt'
        tif_file_name = group_name+'.tif'
        text_destination = destination+text_file_name
        tif_destination = destination+tif_file_name
        try:
            create_text_from_lines(text_destination,text_lines)
        except:
            print("metadata text file not created",group_name+'.txt')
        try:
            create_tif_from_array(tif_destination,tif_array)
        except:
            print("tif file not created",group_name+'.tif')

def unpack_hdf5_tiff_multiple(pathToHDF5,destination):
    f = h5py.File(pathToHDF5,'r+')
    items = f.items()
    group_name = items[0][0]
    group = f[group_name]
    text_lines = group['txt'].value
    tif_array = group['tif'].value
    text_file_name = group_name+'.txt'
    tif_file_name = group_name+'.tif'
    text_destination = destination+text_file_name
    tif_destination = destination+tif_file_name
    try:
        create_text_from_lines(text_destination,text_lines)
    except:
        print("metadata text file not created", group_name+'.txt')
    try:
        create_tif_from_array(tif_destination,tif_array)
    except:
        print("tif file not created",group_name+'.tif')



example = h5py.File('example.hdf5','w')

example.create_group('places')
example['places'].create_group('asia')
example['places'].create_group('europe')
example['places'].create_group('americas')

example['places']['americas'].create_group('north america')
example['places']['americas'].create_group('south america')

example['places']['americas']['north america'].create_dataset('nations',data=['canada','mexico','united states'])

example['places']['asia'].create_group('china')
example['places']['asia']['china'].create_group('mainland')
example['places']['asia']['china'].create_group('taiwan')


mainland_cities = ['shanghai', 'beijing', 'dalian', 'shenzhen']
taiwan_cities = ['taipei','changua','tainan']

example['places']['asia']['china']['mainland'].create_dataset('cities',data=mainland_cities)
example['places']['asia']['china']['taiwan'].create_dataset('cities',data=taiwan_cities)

european_nations = ['france','germany','poland','spain','italy']

example['places']['europe'].create_dataset('nations',data=european_nations)

first_level = example.items()[0]

first_level_name = first_level[0]
first_level_value = first_level[1]
print(type(first_level_name))
print(type(first_level_value))

example.close()

tiff_folder = '/Users/todd_n/Desktop/tiff_test_files/tif-1/'
tiff_folder_contents = os.listdir(tiff_folder)

tiff_file = None
text_file = None
for each in tiff_folder_contents:
    if '.tif' in each:
        tiff_file = tiff_folder+each
    elif '.txt' in each:
        text_file = tiff_folder+each

createHDF5_Tiff('test.hdf5',tiff_file,text_file)

tiff_folder = '/Users/todd_n/Desktop/tiff_test_files/tif-2/'
tiff_folder_contents = os.listdir(tiff_folder)

tiff_file = None
text_file = None
for each in tiff_folder_contents:
    if '.tif' in each:
        tiff_file = tiff_folder+each
    elif '.txt' in each:
        text_file = tiff_folder+each

createHDF5_Tiff('test.hdf5',tiff_file,text_file)



unpack_hdf5_tiff('test.hdf5','/Users/todd_n/Desktop/destination/')


#f = h5py.File('test.hdf5','r+')

#the code below writes a tiff file as an array in a hdf5 file


"""
text_folder = '/Users/todd_n/Desktop/text_files/'
text_files = os.listdir(text_folder)



tif_array = get_tiff_as_array(tiff_file)

tf = open(text_file,'r')
tf_lines = tf.readlines()
tf_lines.insert(0,text_file)
tf_lines.insert(0,'.txt')
"""





"""
f.create_group("tiff_group")
f["tiff_group"].create_dataset("tiff_file",data=tif_array)
f["tiff_group"].create_dataset("text_metadata",data=tf_lines)
f.close()
"""



#this code writes text files to this hdf5 file
"""


files = []

for each in text_files:
    current_path = text_folder+each
    if 'txt' in current_path:
        f1 = open(each,'rb')
        lines = f1.readlines()
        lines.insert(0,each)
        files.append(lines )

entries = create_dataset_text(files,f)

f.create_group("text_files")

f["text_files"].create_dataset("first",data=entries[0])
f["text_files"].create_dataset("second",data=entries[1])
f["text_files"].create_dataset("third",data=entries[2])



the_items = f.items()
"""



