__author__ = 'todd_n'
import h5py
import os
import tifffile
import numpy as np


pathToHDF5 = '/Users/todd_n/t2-c2-extractors/hdf5-helper/test.hdf5'

f = h5py.File('test.hdf5','r+')

top_level = f.items()

group_names = []
for each in top_level:
    current_group = each
    group_names.append(each[0])

first_group_name = group_names[0]

first_group = f[first_group_name]

first_group_items = first_group.items()

first_group_names = []

for each in first_group_items:
    first_group_names.append(each[0])

first_group_content = []

for each in first_group_names:
    current_content = first_group[each].value
    first_group_content.append(current_content)



tiff_group = f["tiff_group"]

tiff_group_items = tiff_group.items()

tiff_file_dataset = f["tiff_group"]["tiff_file"]

tiff_metadata_text_dataset = f["tiff_group"]["text_metadata"]

tiff_metadata_ = tiff_metadata_text_dataset.value[2:]

tiff_file_array = tiff_file_dataset.value

tifffile.imsave('/Users/todd_n/Desktop/test_tiff_file.tif',data=tiff_file_array)

print(type(tiff_group))