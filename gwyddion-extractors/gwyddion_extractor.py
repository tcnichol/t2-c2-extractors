import gwy
import tempfile
import sys

test_afm_file = "/Users/todd_n/Desktop/RE__AFM_X-ray/AFM_GLAM0000.ibw"

mrl_folder = "/Users/todd_n/Desktop/RE__AFM_X-ray"


def create_image_previews(fpath):
    num_previews = len(gwy.gwy_app_data_browser_get_data_ids(fpath))
    container = gwy.gwy_file_load(test_afm_file, gwy.RUN_NONINTERACTIVE)


def create_image_preview(container, current_channel):
    data_field_name = gwy.gwy_app_get_data_field_title(container,current_channel)
    field_thumbnail = gwy.gwy_app_get_channel_thumbnail(container,current_channel,1000,1000)
    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    field_thumbnail.save(tn_file,'png')

    return tn_file

def create_image_preview_from_file(filename, current_channel):
    container = gwy.gwy_file_load(filename,gwy.RUN_NONINTERACTIVE)
    data_field_name = gwy.gwy_app_get_data_field_title(container,current_channel)
    field_thumbnail = gwy.gwy_app_get_channel_thumbnail(container,current_channel,1000,1000)
    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    field_thumbnail.save(tn_file,'png')

    return tn_file

#f = open(test_afm_file,'r')


result = gwy.gwy_file_load(test_afm_file,gwy.RUN_NONINTERACTIVE)

ids = gwy.gwy_app_data_browser_get_data_ids(result)

#test = create_image_preview(result,0)

zero  = result['/0/data']
one = result['/1/data']
two = result['/2/data']
three = result['/3/data']

a1 = gwy.gwy_app_get_data_field_title(result, 0)
a2 = gwy.gwy_app_get_data_field_title(result, 1)
a3 = gwy.gwy_app_get_data_field_title(result, 2)
a4 = gwy.gwy_app_get_data_field_title(result, 3)


b0 = gwy.gwy_app_get_channel_thumbnail(result, 0,1000,1000)
b1 = gwy.gwy_app_get_channel_thumbnail(result, 1,1000,1000)
b2 = gwy.gwy_app_get_channel_thumbnail(result, 2,1000,1000)
b3 = gwy.gwy_app_get_channel_thumbnail(result, 3,1000,1000)


settings = gwy.gwy_app_settings_get()

b0.save('test0.png','png')
#b1.save('test1.png','png',{"quality":"100"})
#b2.save('test2.png','png',{"quality":"100"})
#b3.save('test3.png','png',{"quality":"100"})


print('done')

#f.close()