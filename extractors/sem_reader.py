__author__ = 'todd_n'

import tifffile
import json
import chardet
import tempfile
import os

def convert_encoding(data, new_encoding="UTF-8"):
    if data is None or data == "":
        return "EMPTY"
    current_encoding = chardet.detect(data)['encoding']
    if type(current_encoding) is None:
        #print("is none")
        pass
    if new_encoding.upper() != current_encoding.upper():
        data = data.decode(current_encoding).encode("UTF-8")

    return data

def extract_sem_metadata(fileName):
    metadataFile = metadata_tags(fileName)
    return metadataFile


def metadata_tags(fileName):
    metadataFile = fileName.replace('.tiff','.txt')
    return metadataFile

def get_metadata_from_textfile(metadata_textFile):
    f = open(metadata_textFile, 'r')
    metadata = f.read()
    return metadata

def add_metadata_tags(tiffFile, metadata_textFile):
    metadata_tags = get_metadata_from_textfile(metadata_textFile)

    with tifffile.TiffFile(tiffFile) as tif:
        data = tif.asarray()
        first_page = tif[0]
        try:
            image_description = tif[0].image_description
        except:
            print('does not have image description')
        print(type(first_page))
        tag_values = dict()
        first_page_tags = first_page.tags
        print(type(first_page_tags))
        for tag in first_page.tags.values():
            t = tag.name
            v = tag.value
            tag_values[t] = v
        #need to encode in UTF-8
        test_metadata_tags = "silly sally willy wally"
        new_metadata_tags = convert_encoding(metadata_tags)
        new_description = json.dumps(new_metadata_tags)
        #tif[0].image_description = new_description
        new_data = tif.asarray()
        (fd, tmpfile)=tempfile.mkstemp(suffix='.tif')
        os.close(fd)
        tifffile.imsave(tmpfile,new_data,description=new_description)
        return tmpfile
"""
pathToTiff = '/Users/todd_n/Desktop/MRL/JEOL 7000F SEM/SEM/sem_test/Mag Calibration Test 2.tif'

pathToText = '/Users/todd_n/Desktop/MRL/JEOL 7000F SEM/SEM/sem_test/Mag Calibration Test 2.txt'

temp_tiff_file = add_metadata_tags(pathToTiff,pathToText)

image_description = ''
with tifffile.TiffFile(temp_tiff_file) as temp_tiff:
    data = temp_tiff.asarray()
    first_page = temp_tiff[0]
    try:
        image_description = temp_tiff[0].image_description
    except:
        print('no image description')
    tag_values = dict()
    first_page_tags = first_page.tags
    print(type(first_page_tags))
    for tag in first_page.tags.values():
        t = tag.name
        v = tag.value
        tag_values[t] = v
    print('found them')

f = open('/Users/todd_n/Desktop/text_for_tiff.txt', 'wb')

text = image_description.replace('\\r\\n', '\n')

f.write(text)
f.close()

os.remove(temp_tiff_file)
"""