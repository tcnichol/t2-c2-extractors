__author__ = 'todd_n'
import routes_config
import requests
import json
import upload

def get_content_from_response(response):
    content = response.content
    return content

def view_collections():
    headers={'Content-Type': 'application/json'}
    host = routes_config.host
    url = host+'api/collections'
    r = requests.get(url,headers=headers)
    r.raise_for_status()
    return r

def view_datasets():
    headers={'Content-Type': 'application/json'}
    host = routes_config.host
    url = host+'api/datasets'
    r = requests.get(url,headers=headers)
    r.raise_for_status()
    return r

def view_files():
    headers = {'Content-Type': 'application/json'}
    host = routes_config.host
    url = host+'api/files'

    r = requests.get(url,headers=headers)
    r.raise_for_status()
    return r

def create_empty_collection(current_name,current_description):
    headers={'Content-Type': 'application/json'}
    host = routes_config.host
    url = host+'api/collections?key='+routes_config.play_server_key
    body = dict()
    body['name'] = current_name
    body['description'] = current_description
    data = json.dumps(body)
    r = requests.post(url,headers = headers,data=data)
    r.raise_for_status()
    return r

def create_empty_dataset(current_name,current_description):
    headers={'Content-Type': 'application/json'}
    host = routes_config.host
    url = host+'api/datasets/createempty?key='+routes_config.play_server_key
    body = dict()
    body['name'] = current_name
    body['description'] = current_description
    data = json.dumps(body)
    r = requests.post(url,headers = headers,data=data)
    r.raise_for_status()
    return r

def post_user_metadata(metadata,fileid):
    headers={'Content-Type': 'application/json'}
    body = json.dumps(metadata)
    url = routes_config.host+'api/files/'+fileid+'/usermetadata?key='+routes_config.play_server_key
    r = requests.post(url,headers=headers,data=body)
    r.raise_for_status()
    return r

def post_system_metadata(metadata,fileid):
    headers={'Content-Type': 'application/json'}
    body = json.dumps(metadata)
    url = routes_config.host+'api/files/'+fileid+'/metadata?key='+routes_config.play_server_key
    r = requests.post(url,headers=headers,data=body)
    r.raise_for_status()
    return r

def upload_file(file_path):
    file_id = upload.upload_file(file_path)
    return file_id

def delete_file(file_id):
    url = routes_config.host+'api/files/'+file_id+'?key='+routes_config.play_server_key
    r = requests.delete(url)
    r.raise_for_status()
    return r

def delete_dataset(dataset_id):
    url = routes_config.host+'api/datasets/'+dataset_id+'?key='+routes_config.play_server_key
    r = requests.delete(url)
    r.raise_for_status()
    return r

def new_delete_dataset(dataset_id):
    url = routes_config.host+'api/datasets/'+dataset_id

def delete_collection(collection_id):
    url = routes_config.host+'api/collections/'+collection_id+'?key='+routes_config.play_server_key
    r = requests.delete(url)
    r.raise_for_status()
    return r

def delete_anonymous_files():
    files = view_files()
    json_content = json.loads(files.content)
    for each in json_content:
        current_authorId = each['authorId']
        if current_authorId == 'anonymous' or current_authorId == '':
            current_fileid = str(each['id'])
            delete_file(current_fileid)

def delete_anonymous_datasets():
    datasets = view_datasets()
    json_content = json.loads(datasets.content)
    for each in json_content:
        current_authorId = each['authorId']
        if current_authorId == 'anonymous' or current_authorId == '':
            current_fileid  = str(each['id'])
            delete_dataset(current_fileid)

def delete_anonymous_collections():
    collections = view_collections()
    json_content = json.loads(collections.content)
    for each in json_content:
        try:
            current_authorId = each['author']
            if current_authorId == 'anonymous' or current_authorId == '' or current_authorId == "None":
                current_fileid = str(each['id'])
                delete_collection(current_fileid)
        except:
            pass

def delete_all_files():
    files = view_files()
    json_content = json.loads(files.content)
    for each in json_content:
        try:
            current_fileid = str(each['id'])
            delete_file(current_fileid)
        except:
            pass

def delete_all_datasets():
    datasets = view_datasets()
    json_content = json.loads(datasets.content)
    for each in json_content:
        try:
            current_datasetid = str(each['id'])
            delete_dataset(current_datasetid)
        except:
            pass

def delete_all_collections():
    collections = view_collections()
    json_content = json.loads(collections.content)
    for each in json_content:
        try:
            current_collectionid = str(each['id'])
            delete_collection(current_collectionid)
        except:
            pass

def delete_everything():
    delete_all_files()
    delete_all_datasets()
    delete_all_collections()

files = view_files()

#this has the response, we would really just want the content as a json
"""
datasets = view_datasets()

collections = view_collections()

#delete_anonymous_collections()
delete_anonymous_datasets()



uploaded_file_id = upload_file('/Users/todd_n/Desktop/test_image.jpg')

#deleted_r = delete_file(uploaded_file_id)
"""


#

#the code below actual posts data, so it is commented out

"""
empty_collection = create_empty_collection('some name','some description')
empty_dataset = create_empty_dataset('some name','some description')

user_metadata = dict()

user_metadata['user key 1'] = 'user value 1'
user_metadata['user key 2'] = 'user value 2'

system_metadata = dict()

system_metadata['system key 1'] = 'system value 1'
system_metadata['system key 2'] = 'system value 2'

user_metadata_test = post_user_metadata(dict(user_metadata),routes_config.sample_fileid)
system_metadata_test = post_system_metadata(dict(system_metadata),routes_config.sample_fileid)
"""