#!/usr/bin/env python
#  -*- coding: utf-8 -*-
"""
Author: Brock Angelo <jba@illinois.edu>
Description: This is a demo python script that can be run from the command line
to automate uploading a file to an API.
Run this script with the '--help' argument to view the available options:
    "python upload.py --help"
Run this script with the '--file' argument to upload a local file to an API:
	"python upload.py --file /path/to/file"
	(where /path/to/file is a real path to a file on your local filesystem)
NOTE: a successful upload of a file won't log anything to the command line
unless you modify this script. You'll only see an alert if it fails.
You'll need to install the dependencies before this script can run:
	"pip install argparse"
	"pip install requests"
	"pip install logging"
Please see documentation in-line below.
"""

import argparse
import requests
import logging
import tempfile
import os

global r, api_base

# set the url of the API, and credentials here
#api_base = "http://imlczo-dev.ncsa.illinois.edu/clowder/api/"
#username = "josh@example.com"
#password = "password"
api_base = 'http://localhost:9000/api/'
username = 'god@god.com'
password = 'godgodgod'


# this creates the HTTP session used to post to the API
r = requests.Session()
r.auth = (username, password)


# this is the only function that gets called when you run the script
def upload_file(path_to_file):

    # this uses the session with your credentials to actually
    # POST the file to the API
    #(fd, tn_file) = tempfile.mkstemp(suffix=".hdf5")
    #os.close(fd)
    response = r.post('%sfiles' % api_base, files={'File': open(path_to_file, 'rb')})

    # if it wasn't successful, alert and exit
    if response.status_code != 200:
        logging.error('Problem uploading file  : [%d] - %s)' %
            (response.status_code, response.text))
        return None

    # if it was successful, we'll return the ID of the file on the API
    file_id = str(response.json()['id'])
    return file_id



# this is python-lingo for "if I run this script from the command line"
if __name__ == '__main__':

    # this sets up our "argument parser" so we can pass arguments from the command line
    parser = argparse.ArgumentParser()

    # this next block says that we are expecting you to specify the path
    # to the file each time you run this script
    # by adding "--file /path/to/file"
    parser.add_argument(
        '--file',
        help="The local path to the file you want to upload",
        nargs='?',
        required=True
    )

    # this actually parses the arguments
    args = parser.parse_args()

    # this tells the script to use the DEBUG level of logging (it will log everything)
    # add this somewhere in the script if you want to log more things: logging.debug('some text')
    logging.basicConfig(level=logging.DEBUG)

    # finally, we call the upload function using the file passed in as an argument
    upload_file(args.file)
