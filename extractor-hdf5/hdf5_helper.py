__author__ = 'todd_n'
import os
import h5py
import tifffile
import tempfile
import chardet
import re

def get_tiff_as_array(pathToTiff):
    tif = tifffile.TiffFile(pathToTiff)
    tif_array = tif.asarray()
    return tif_array

def get_base_name_from_path(pathToFile):
    path_parts = pathToFile.split('/')
    file_name = path_parts[-1]
    file_name = file_name[:file_name.index('.')]
    return file_name

def get_file_name_from_path(pathToFile):
    path_parts = pathToFile.split('/')
    file_name = path_parts[-1]
    return file_name

def get_extension_from_filename(filename):
    extension = ""
    for i in range(len(filename)-1,0,-1):
        character = filename[i]
        if character == '.':
            extension = filename[i:]
            break
    return extension

def get_extension_from_path(pathToFile):
    path_as_list = pathToFile.split('/')
    filename =path_as_list[-1]
    extension = get_extension_from_filename(filename)
    return extension

def createHDF5_Tiff_temp(pathToHDF5,pathToTiff, pathToMetadataFile):
    (fd, tn_file) = tempfile.mkstemp(suffix=".hdf5")
    os.close(fd)


    if os.path.isfile(pathToHDF5):
        f = h5py.File(pathToHDF5,'r+')
    else:
        f = h5py.File(pathToHDF5,'w')
    tif_array = get_tiff_as_array(pathToTiff)
    base_name = get_base_name_from_path(pathToTiff)
    meta_f = open(pathToMetadataFile,'r')
    lines = meta_f.readlines()

    meta_f.close()

    try:
        f.create_group(base_name)
        f[base_name].create_dataset(base_name+'.tif',data=tif_array)
        f[base_name].create_dataset(base_name+'.txt',data=lines)
        f.close()
    except:
        print("could not create group")
    try:
        f[base_name].create_dataset('tif',data=tif_array)
        f[base_name].create_dataset('txt',data=lines)
        f.close()
    except:
        print("could not overwrite group")
    return tn_file

def createHDF5_fromImageFile(pathToHDF5,pathToImageFile,pathToMetadataFile):
    f = None
    if os.path.isfile(pathToHDF5):
        f = h5py.File(pathToHDF5,'r+')
    else:
        f = h5py.File(pathToHDF5,'w')

    imageFile = open(pathToImageFile,'rb')
    imageFile_bytes = bytearray(imageFile.read())
    imageFileName = get_file_name_from_path(pathToImageFile)

    metadataFile = open(pathToMetadataFile,'rb')
    metadataFile_bytes = bytearray(metadataFile.read())
    metadataFileName = get_file_name_from_path(pathToMetadataFile)

    f.create_dataset(imageFileName,data=imageFile_bytes)
    f.create_dataset(metadataFileName,data=metadataFile_bytes)

    items = f.items()

    image_dataset_name = None
    metadata_dataset_name = None
    for i in range(0,len(items)):
        dataset_name = items[i][0]
        if 'txt' in dataset_name:
            metadata_dataset_name = items[i][0]
        else:
            image_dataset_name = items[i][0]

    f.close()
    return f


def createHDF5_Tiff(pathToHDF5,pathToTiff, pathToMetadataFile):
    f = None
    if os.path.isfile(pathToHDF5):
        f = h5py.File(pathToHDF5,'r+')
    else:
        f = h5py.File(pathToHDF5,'w')


    tif_array = get_tiff_as_array(pathToTiff)
    base_name = get_base_name_from_path(pathToTiff)
    meta_f = open(pathToMetadataFile,'r')
    lines = meta_f.readlines()

    meta_f.close()

    try:
        f.create_group(base_name)
        f[base_name].create_dataset(base_name+'.tif',data=tif_array)
        f[base_name].create_dataset(base_name+'.txt',data=lines)
        f.close()
    except:
        print("could not create group")
    try:
        f[base_name].create_dataset(base_name+'.tif',data=tif_array)
        f[base_name].create_dataset(base_name+'.txt',data=lines)
        f.close()
    except:
        print("could not overwrite group")
    return f

def get_tiff_as_array(pathToTif):
    tif = tifffile.TiffFile(pathToTif)
    tif_array = tif.asarray()
    return tif_array

def get_tiff_from_hdf5_returns_tempfile(pathToHDF5):
    f = h5py.File(pathToHDF5,'r+')
    items = f.items()
    group_name = items[0][0]
    tif_as_array = f[group_name][group_name+'.tif']
    (fd, tmpfile)=tempfile.mkstemp(suffix='.tif')
    os.close(fd)
    #create temp file and write tif array as data
    tifffile.imsave(tmpfile,data=tif_as_array)
    return tmpfile

def convert_encoding(data, new_encoding="UTF-8"):
    if data is None or data == "":
        return "EMPTY"
    current_encoding = chardet.detect(data)['encoding']
    if type(current_encoding) is None:
        print("is none")
    if new_encoding.upper() != current_encoding.upper():
        data = data.decode(current_encoding).encode("UTF-8")

    return data

def get_dict_from_lines(lines_from_hdf5):

    metadata = dict()

    for line in lines_from_hdf5:
        current = line
        current = current.split(' ')
        key = current[0]
        value = line[len(key)+1:]
        value=value.rstrip('\r\n')
        new_key = convert_encoding(key,new_encoding="UTF-8")
        new_value = convert_encoding(value,new_encoding="UTF-8")
        metadata[new_key] = new_value

    return metadata

def unpack_hdf5(pathToHDF5):
    f = h5py.File(pathToHDF5,'r+')
    items = f.items()
    image_file_name = None
    image_dataset_value = None

    metadata_file_name = None
    metadata_dataset_value = None
    for i in range(0,len(items)):
        dataset_name = items[i][0]
        if 'txt' in dataset_name:
            metadata_file_name = items[i][0]
            metadata_dataset_value = f[items[i][0]].value
        else:
            image_file_name = items[i][0]
            image_dataset_value = f[items[i][0]].value

    f.close()
    return [[metadata_file_name,metadata_dataset_value],[image_file_name,image_dataset_value]]

def get_image_tempfile_from_unpacked_hdf5(unpacked_hdf5):
    imagefile_name = unpacked_hdf5[1][0]
    file_extension = get_extension_from_path(imagefile_name)
    image_value = unpacked_hdf5[1][1]

    (fd, tmpfile)=tempfile.mkstemp(suffix=file_extension)
    os.close(fd)
    f = open(tmpfile,'wb')
    f.write(bytearray(image_value))
    return tmpfile

def write_image_from_unpacked_hdf5(unpacked_hdf5,target):
    imagefile_name = unpacked_hdf5[1][0]
    image_value = unpacked_hdf5[1][1]
    f = open(target+imagefile_name,'wb')
    f.write(bytearray(image_value))
    f.close()

def get_metadata_dict_from_unpacked_hdf5(unpacked_hdf5):
    metadata = unpacked_hdf5[0][1]
    data = str(bytearray(metadata))
    lines = []
    if '\r\n' in data:
        lines = data.split('\r\n')
    elif '\n' in data:
        lines = data.split('\n')
    metadata = dict()
    for line in lines:
        try:
            raw_key = line[:line.index(' ')]
            raw_value = line[line.index(' ')+1:]
            raw_key = raw_key.replace('$','')
            raw_key.replace('.','_') # more testing needed to see what cannot be in json
            key = convert_encoding(raw_key,new_encoding='utf-8')
            value = convert_encoding(raw_value,new_encoding='utf-8')
            metadata[key] = value
        except BaseException as e:
            str(e)
    return metadata

def get_metadata_from_hdf5_returns_dictionary(pathToHDF5):
    f = h5py.File(pathToHDF5,'r+')
    items = f.items()
    group_name = items[0][0]
    text_as_lines = f[group_name][group_name+'.txt'].value
    return text_as_lines

sampleTif = '/Users/todd_n/Desktop/tiff_test_files/tif-1/Mag Calibration Test 2.tif'
sampleMetadata = '/Users/todd_n/Desktop/tiff_test_files/tif-1/Mag Calibration Test 2.txt'

sample_png = '/Users/todd_n/Desktop/tiff_test_files/png_test/Mayakovsky-Rosta-Window-No.14.png'
png_metadata = '/Users/todd_n/Desktop/tiff_test_files/png_test/png_metadata.txt'

sample_jpg = '/Users/todd_n/Desktop/tiff_test_files/jpg_test/julie-speed-head.jpg'
jpg_metadata = '/Users/todd_n/Desktop/tiff_test_files/jpg_test/jpg_metadata.txt'

"""
ext1 = get_extension_from_path(sampleTif)
bn = get_base_name_from_path(sampleTif)
ext = get_extension_from_path('Mag calibration test.tif')
print(ext)
"""

#createHDF5_fromImageFile('test_file.hdf5',sampleTif,sampleMetadata)

#createHDF5_fromImageFile('test_png_file.hdf5',sample_png,png_metadata)
#createHDF5_fromImageFile('test_jpg_file.hdf5',sample_jpg,jpg_metadata)


#result_png_unpacked = unpack_hdf5('test_png_file.hdf5')

#result_jpg_unpacked = unpack_hdf5('test_jpg_file.hdf5')

#write_image_from_unpacked_hdf5(result_png_unpacked,'/Users/todd_n/Desktop/test/')

#write_image_from_unpacked_hdf5(result_jpg_unpacked,'/Users/todd_n/Desktop/test/')


"""
result = unpack_hdf5('/Users/todd_n/Desktop/test_file.hdf5')

m_value = get_metadata_dict_from_unpacked_hdf5(result)

metadata = result[0]
metadata_name = metadata[0]
metadata_value = metadata[1]
image = result[1]
image_name = image[0]
image_value = image[1]
"""
